import React from "react";
import "./SidebarOption.css";

function SidebarOption({ icon, title, number, selected }) {
  return (
    <div className={`sidebar-option ${selected && "sidebar-active"}`}>
      {icon}
      <h3>{title}</h3>
      <p>{number}</p>
    </div>
  );
}

export default SidebarOption;
