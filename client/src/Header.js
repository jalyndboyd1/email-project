import React from "react";
import "./Header.css";
import MenuIcon from "@material-ui/icons/Menu";
import { Avatar, IconButton } from "@material-ui/core";
import logo from "./assets/duckie-logo.jpg";
import SearchIcon from "@material-ui/icons/Search";
import ArrowDropDownCircleIcon from "@material-ui/icons/ArrowDropDownCircle";
import AppsIcon from "@material-ui/icons/Apps";
import NotificationsActiveIcon from "@material-ui/icons/NotificationsActive";

function Header() {
  return (
    <div className="header">
      <div className="header-left">
        <IconButton>
          <MenuIcon />
        </IconButton>
        <img src={logo} alt="logo" />
      </div>
      <div className="header-middle">
        <SearchIcon />
        <input placeholder="Search Mail" />
        <ArrowDropDownCircleIcon className="header-drop" />
      </div>
      <div className="header-right">
        <IconButton>
          <AppsIcon />
        </IconButton>
        <IconButton>
          <NotificationsActiveIcon />
        </IconButton>
        <IconButton>
          <Avatar />
        </IconButton>
      </div>
    </div>
  );
}

export default Header;
