import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCr6CmQFaREJlYelIvRw4zXrJdbHg7n-I4",
  authDomain: "email-project-24c75.firebaseapp.com",
  projectId: "email-project-24c75",
  storageBucket: "email-project-24c75.appspot.com",
  messagingSenderId: "925910408536",
  appId: "1:925910408536:web:1489e4484d15864a8d592d",
  measurementId: "G-QHJ44L937Q",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

export default db;
