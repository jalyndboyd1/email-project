import { Button, IconButton } from "@material-ui/core";
import React from "react";
import "./Sidebar.css";
import SidebarOption from "./SidebarOption";
import InboxIcon from "@material-ui/icons/Inbox";
import AddIcon from "@material-ui/icons/Add";
import StarIcon from "@material-ui/icons/Star";
import SnoozeIcon from "@material-ui/icons/Snooze";
import LabelImportantIcon from "@material-ui/icons/LabelImportant";
import SendIcon from "@material-ui/icons/Send";
import PanToolIcon from "@material-ui/icons/PanTool";
import PersonIcon from "@material-ui/icons/Person";
import PhoneIcon from "@material-ui/icons/Phone";

function Sidebar() {
  return (
    <div className="sidebar">
      <Button
        startIcon={<AddIcon fontSize="large" />}
        className="sidebar-compose"
      >
        Compose
      </Button>
      <SidebarOption
        icon={<InboxIcon />}
        title="Inbox"
        number={30}
        selected={true}
      />
      <SidebarOption icon={<SnoozeIcon />} title="Snoozed" number={5} />
      <SidebarOption
        icon={<LabelImportantIcon />}
        title="Important"
        number={6}
      />
      <SidebarOption icon={<SendIcon />} title="Sent" number={3} />
      <SidebarOption icon={<PanToolIcon />} title="Spam" number={150} />
      <div className="sidebar-footer">
        <div className="sidebar-footerIcons">
          <IconButton>
            <PersonIcon />
          </IconButton>
          <IconButton>
            <PhoneIcon />
          </IconButton>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
